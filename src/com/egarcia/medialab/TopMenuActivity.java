package com.egarcia.medialab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TopMenuActivity extends Activity {
	Button mSongs, mVideos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_top_menu);
		getViews();
		setListeners();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.top_menu, menu);
		return false;
	}
	
	void getViews() {
		mSongs  = (Button) this.findViewById(R.id.TopMenu_btn_songs);
		mVideos = (Button) this.findViewById(R.id.TopMenu_btn_videos);
				
	}
	
	void setListeners() {
		mSongs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(TopMenuActivity.this, SongViewPagerActivity.class);
				startActivity(i);
			}
			
		});
		mVideos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(TopMenuActivity.this, VideoFragmentManagerActivity.class);
				startActivity(i);
			}
			
		});
		
	}

}
