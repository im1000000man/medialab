package com.egarcia.medialab.Models;

public class Video {
	String mTitle;
	String mYoutubeId;
	
	public Video(String mTitle, String mYoutubeId) {
		super();
		this.mTitle = mTitle;
		this.mYoutubeId = mYoutubeId;
	}
	public String getmTitle() {
		return mTitle;
	}
	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}
	public String getmYoutubeId() {
		return mYoutubeId;
	}
	public void setmYoutubeId(String mYoutubeId) {
		this.mYoutubeId = mYoutubeId;
	}
}
