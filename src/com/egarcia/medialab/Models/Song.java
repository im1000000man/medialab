package com.egarcia.medialab.Models;


public class Song {
	String mSongTitle;
	String mArtist;
	String mAlbum;
	int mSongFileResID, mSongCoverResID;

	public String getmSongTitle() {
		return mSongTitle;
	}

	public void setmSongTitle(String mSongTitle) {
		this.mSongTitle = mSongTitle;
	}

	public String getmArtist() {
		return mArtist;
	}

	public void setmArtist(String mArtist) {
		this.mArtist = mArtist;
	}

	public String getmAlbum() {
		return mAlbum;
	}

	public void setmAlbum(String mAlbum) {
		this.mAlbum = mAlbum;
	}

	public int getmSongFileUri() {
		return mSongFileResID;
	}

	public void setmSongFileUri(int mSongFileUri) {
		this.mSongFileResID = mSongFileUri;
	}

	public int getmSongCoverUri() {
		return mSongCoverResID;
	}

	public void setmSongCoverUri(int mSongCoverUri) {
		this.mSongCoverResID = mSongCoverUri;
	}

	// Constructor completo
	public Song(String mSongTitle, String mArtist, String mAlbum,
			int mSongFileUri, int mSongCoverUri) {
		super();
		this.mSongTitle = mSongTitle;
		this.mArtist = mArtist;
		this.mAlbum = mAlbum;
		this.mSongFileResID = mSongFileUri;
		this.mSongCoverResID = mSongCoverUri;
	}

	// Constructor mínimo
	public Song(String mSongTitle, int mSongFileUri) {
		super();
		this.mSongTitle = mSongTitle;
		this.mSongFileResID = mSongFileUri;
	}

	

}
