package com.egarcia.medialab.Models;

import java.util.ArrayList;

import com.egarcia.medialab.R;




public class SongsDB {
	ArrayList <Song> songsdb;

	public SongsDB() {
		super();
		songsdb = new ArrayList <Song>();
		songsdb.add(new Song("Electro Man","Calvin Harris", "I Created Disco",R.raw.calvin_harris_electro_man, 0));
		songsdb.add(new Song("SkyFall", "Adele", "Bond... James Bond", R.raw.adele_sky_fall, 0));
		songsdb.add(new Song("World, Hold On", "Bob Sinclair", "Western Dream", R.raw.bob_sinclar_world_hold_on,0));
	}

	public ArrayList<Song> getSongsdb() {
		return songsdb;
	}

	public void setSongsdb(ArrayList<Song> songsdb) {
		this.songsdb = songsdb;
	}
	
	
	
}
