package com.egarcia.medialab.Adapters;

import java.util.List;

import com.egarcia.medialab.Models.Video;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
// se usa BaseAdapter en lugar de Array adapter por lo siguiente:
//http://www.piwai.info/android-adapter-good-practices/

public class VideoListAdapter extends BaseAdapter {
    private List<Video> mListHeaders; 
    private Context mContext;
    
    public VideoListAdapter(List<Video> mHeaders, Context context){
    	this.mListHeaders = mHeaders;    
    	this.mContext = context;
    }
    
	@Override
	public int getCount() {
		return mListHeaders.size();
	}

	@Override
	public Object getItem(int position) {
		return mListHeaders.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = new TextView(mContext);
		}
		((TextView) convertView).setText(mListHeaders.get(position).getmTitle());
		((TextView) convertView).setTextAppearance(mContext, android.R.attr.textAppearanceLarge);
		return convertView;
	}
	

	@Override
	public boolean isEmpty() {
		return mListHeaders.isEmpty();
	}

	
}
