package com.egarcia.medialab.Adapters;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SongPagerAdapter extends FragmentPagerAdapter {

	private List<Fragment> fragments;
	public SongPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
		super(fm);
		this.fragments = fragments;
	}
	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}
	@Override
	public int getCount() {
		return this.fragments.size();
	}
	


}
