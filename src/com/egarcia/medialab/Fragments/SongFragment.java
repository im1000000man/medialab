package com.egarcia.medialab.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.egarcia.medialab.R;
import com.egarcia.medialab.SongPlayer;
import com.egarcia.medialab.Models.Song;

public class SongFragment extends Fragment {

	private SongPlayer mPlayer = new SongPlayer();

	private ImageButton mPlayButton;
	private String mSongTitle;
	private String mAlbum;
	private String mArtist;
	int mSongFileResID, mSongCoverResID;

	@Override
	public void onDestroy() {
		super.onDestroy();
		mPlayer.stop();
	}

	// Seguimos las recomendaciones de clase para nombrar extras:
	public static final String EXTRA_MESSAGE_TITLE = "COM.EGARCIA.MEDIALAB.EXTRA_MESSAGE_SONG_TITLE";
	public static final String EXTRA_MESSAGE_ID = "COM.EGARCIA.MEDIALAB.EXTRA_MESSAGE_SONG_ID";
	public static final String EXTRA_MESSAGE_ARTIST = "COM.EGARCIA.MEDIALAB.EXTRA_MESSAGE_SONG_ARTIST";
	public static final String EXTRA_MESSAGE_ALBUM = "COM.EGARCIA.MEDIALAB.EXTRA_MESSAGE_SONG_ALBUM";
	public static final String EXTRA_MESSAGE_ID_COVER = "COM.EGARCIA.MEDIALAB.EXTRA_MESSAGE_SONG_ID_COVER";

	/*
	 * Método static para poder crear nuevos fragmentos en otras clases, para
	 * ello, no es estrictamente necesario que sea static, sólo más cómodo por
	 * contener datos constantes. ;) .
	 */
	public static final SongFragment newInstance(Song song) {
		SongFragment f = new SongFragment();
		// Enviamos como parámetro un 1 que es la cantidad de elementos que va a
		// alojar:
		Bundle bdl = new Bundle(2);
		bdl.putString(EXTRA_MESSAGE_TITLE, song.getmSongTitle());
		bdl.putString(EXTRA_MESSAGE_ARTIST, song.getmArtist());
		bdl.putString(EXTRA_MESSAGE_ALBUM, song.getmAlbum());
		bdl.putInt(EXTRA_MESSAGE_ID, song.getmSongFileUri());
		bdl.putInt(EXTRA_MESSAGE_ID_COVER, song.getmSongCoverUri());
		f.setArguments(bdl);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mSongTitle = getArguments().getString(EXTRA_MESSAGE_TITLE);
		mSongFileResID = getArguments().getInt(EXTRA_MESSAGE_ID);
		mArtist = getArguments().getString(EXTRA_MESSAGE_ARTIST);
		mAlbum = getArguments().getString(EXTRA_MESSAGE_ALBUM);
		mSongCoverResID = getArguments().getInt(EXTRA_MESSAGE_ID_COVER);

		View v = inflater.inflate(R.layout.activity_song_fragment, container,
				false);
		TextView mediaTitleTextView = (TextView) v
				.findViewById(R.id.Activity_Song_Fragment_TV_Media_Title);		
		mediaTitleTextView.setText(mSongTitle);
		
		TextView mediaArtistTextView = (TextView) v
				.findViewById(R.id.Activity_Song_Fragment_TV_Media_Artist);		
		mediaArtistTextView.setText(mArtist);
		
		TextView mediaAlbumTextView = (TextView) v
				.findViewById(R.id.Activity_Song_Fragment_TV_Media_Album);		
		mediaAlbumTextView.setText(mAlbum);
		
		
		
		mPlayButton = (ImageButton) v.findViewById(R.id.btnPlay);

		mPlayButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (!mPlayer.isPlaying()) {
					mPlayButton.setImageResource(R.drawable.btn_pause);
					mPlayer.play(getActivity(), mSongFileResID);
				} else {
					mPlayButton.setImageResource(R.drawable.btn_play);
					mPlayer.stop();
				}

			}
		});

		return v;
	}

}
