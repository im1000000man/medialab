package com.egarcia.medialab.Fragments;

import com.egarcia.medialab.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class YouTubePlayerFragment extends
		com.google.android.youtube.player.YouTubePlayerFragment {

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_youtube_player_fragment,
				container, false);

		return v;

	}
	
}
