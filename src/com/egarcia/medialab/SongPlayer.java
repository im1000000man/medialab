package com.egarcia.medialab;

import android.content.Context;
import android.media.MediaPlayer;

public class SongPlayer {

    private MediaPlayer mPlayer;
    
    public void stop() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void play(Context c, int uri) {
        
        stop();

        mPlayer = MediaPlayer.create(c, uri);
        
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                stop();
            }
        });

        mPlayer.start();
    }
    
    public boolean isPlaying() {
        return mPlayer != null;
    }
    
}
