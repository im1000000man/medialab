package com.egarcia.medialab;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import com.egarcia.medialab.Adapters.SongPagerAdapter;
import com.egarcia.medialab.Fragments.SongFragment;
import com.egarcia.medialab.Models.Song;
import com.egarcia.medialab.Models.SongsDB;

public class SongViewPagerActivity extends FragmentActivity {
	SongPagerAdapter pageAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_song_view_pager);
		List<Fragment> fragments = getFragments();
		pageAdapter = new SongPagerAdapter(getSupportFragmentManager(),
				fragments);
		ViewPager pager = (ViewPager) findViewById(R.id.viewpager);
		pager.setAdapter(pageAdapter);
	}

	private List<Fragment> getFragments(){
			SongsDB sdb = new SongsDB();
		  List<Fragment> fList = new ArrayList<Fragment>();	
		  for(Song s: sdb.getSongsdb()) {
			  fList.add(SongFragment.newInstance(s));
		  }		  
		  
		  return fList;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.song_view_pager, menu);
		return false;
	}

}
