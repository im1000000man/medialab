package com.egarcia.medialab;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.egarcia.medialab.Adapters.VideoListAdapter;
import com.egarcia.medialab.Models.Video;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

public class VideoListFragmentActivity extends Fragment {
	private ListView mVideoListView;
	private VideoListAdapter mVideoListAdapter;
	private List<Video> mHeaders;
	// keytool -list -v -keystore ~/debug.keystore
	private String developer_key = "Insert your Dev Key here";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_video_fragment, container,
				false);
		getViews(v);
		setAdapter(v);
		setListeners();

		return v;

	}

	void changeToVideoFragment(final View v, final String youtubeVideoID) {
		try {
			YouTubePlayerSupportFragment mYoutubePlayerFragment = new YouTubePlayerSupportFragment();
			mYoutubePlayerFragment.initialize(developer_key,
					new YouTubePlayer.OnInitializedListener() {

						@Override
						public void onInitializationFailure(Provider provider,
								YouTubeInitializationResult error) {
							Toast.makeText(v.getContext(),
									"Oh no! " + error.toString(),
									Toast.LENGTH_LONG).show();
						}

						@Override
						public void onInitializationSuccess(Provider arg0,
								YouTubePlayer player, boolean wasRestored) {
							player.loadVideo(youtubeVideoID);

						}

					});
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(
					R.id.VideoFragmentParentActivity_FL_fragment_container,
					mYoutubePlayerFragment);
			fragmentTransaction.commit();
		} catch (Exception e) {
			Toast.makeText(v.getContext(), "Ha fallado el servicio reproductor de youtube :( ", Toast.LENGTH_SHORT ).show(); 
		}
	}

	void getViews(View v) {
		mVideoListView = (ListView) v
				.findViewById(R.id.VideoFragmentActivity_lv_video);
	}

	void setAdapter(View v) {
		mHeaders = new ArrayList<Video>();
		prepareList();
		mVideoListAdapter = new VideoListAdapter(mHeaders, v.getContext());
		mVideoListView.setAdapter(mVideoListAdapter);

	}

	void setListeners() {
		mVideoListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				changeToVideoFragment(view, mHeaders.get(position)
						.getmYoutubeId());
			}

		});
	}

	void prepareList() {
		// Normalmente se obtiene de un servidor o BD, por ahora para el lab, lo
		// definiremos localmente
		mHeaders.add(new Video("Como un lobo", "6bowAYq0OLU"));
		mHeaders.add(new Video("Perfecta", "ie3mbivU7QQ"));
		mHeaders.add(new Video("Aire Soy", "hnCQlCqjvqo"));
	}

}
