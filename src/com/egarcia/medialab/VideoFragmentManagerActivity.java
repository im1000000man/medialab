package com.egarcia.medialab;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

public class VideoFragmentManagerActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_fragment_parent);
		// Show the Up button in the action bar.
		setupActionBar();
		setInitialListFragment(savedInstanceState);
	}
	
	
	
	void setInitialListFragment(Bundle savedInstanceState) {
		if (findViewById(R.id.VideoFragmentParentActivity_FL_fragment_container) != null) {
			           
            if (savedInstanceState != null) {
                return;
            }

            VideoListFragmentActivity firstFragment = new VideoListFragmentActivity();            
            //firstFragment.setArguments(getIntent().getExtras());            
            getSupportFragmentManager().beginTransaction().add(R.id.VideoFragmentParentActivity_FL_fragment_container, firstFragment).commit();
            
        }

	}
	


	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.video_fragment_parent, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
